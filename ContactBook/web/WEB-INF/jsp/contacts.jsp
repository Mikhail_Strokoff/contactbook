<%-- 
    Document   : contactbook
    Created on : 13.08.2016, 20:45:51
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <title>Телефонный справочник</title>
    </head>
    <body>
        <h1>Телефонный справочник</h1>
        
        <form method="GET">
            <input placeholder="Поиск по ФИО" name="search" />
            <input type="submit" value="Искать" />
        </form>
        
        <table border="1">
            <tr>
                <th>Имя</th>
                <th>Отчество</th>
                <th>Фамилия</th>
                <th>Телефон</th>
                <th>Адрес</th>
                <th>Рабочий телефон</th>
                <th>Место работы</th>
                <th colspan="2">Действия</th>
            </tr>
            
            <c:forEach var="contact" items="${contacts}">
                <tr>
                <form method="POST">
                    <td hidden="true">
                        <input hidden="true" type="number" value="${contact.getId()}" name="id" />
                    </td>
                    <td>
                        <input value="${contact.getFirstName()}" name="firstName"  />
                    </td>
                    <td>
                        <input value="${contact.getSecondName()}" name="secondName" />
                    </td>
                    <td>
                        <input value="${contact.getLastName()}" name="lastName" />
                    </td>
                    <td>
                        <input value="${contact.getPhone()}" name="phone" />
                    </td>
                    <td>
                        <input value="${contact.getAddress()}" name="address" />
                    </td>
                    <td>
                        <input value="${contact.getWorkPhone()}" name="workPhone" />
                    </td>
                    <td>
                        <input value="${contact.getWorkPlace()}" name="workPlace" />
                    </td>
                    <td>
                        <input type="submit" value="Редактировать" />
                    </td>
                </form>
                <form method="POST">
                    <td hidden="true">
                        <input hidden="true" value="${contact.getId()}" name="id" />
                    </td>
                    <td hidden="true">
                        <input hidden="true" type="checkbox" checked="true" name="isDeleted" />
                    </td>
                    <td>
                        <input type="submit" value="Удалить" />
                    </td>
                </form>
                </tr>
            </c:forEach>
            
            <tr>
            <form method="POST">
                <td hidden="true">
                    <input hidden="true" value="0" name="id" id="newContact"/>
                    <script>{
                        var result = 0;
                        
                        var elemList = document.getElementsByName("id");
                        for(var i = 0; i < elemList.length; i++){
                            var val = elemList.item(i).getAttribute("value");
                            result = result > ++val
                                ? result
                                : val;
                        }
                        
                        document.getElementById("newContact").value = result;
                    }</script>
                </td>
                <td>
                    <input placeholder="Имя" name="firstName" />
                </td>
                <td>
                    <input placeholder="Отчество" name="secondName" />
                </td>
                <td>
                    <input placeholder="Фамилия" name="lastName" />
                </td>
                <td>
                    <input placeholder="Телефон" name="phone" />
                </td>
                <td>
                    <input placeholder="Адрес" name="address" />
                </td>
                <td>
                    <input placeholder="Рабочий телефон" name="workPhone" />
                </td>
                <td>
                    <input placeholder="Место работы" name="workAddress" />
                </td>
                <td colspan="2">
                    <input type="submit" value="Добавить запись" />
                </td>
            </form>
            </tr>
        </table>
        
                <script>
                    
                    function getNextId(){
                        var result = 0;
                        
                        var elemList = document.getElementsByName("id");
                        for(var elem in elemList){
                            result = result > elem.value
                                ? result
                                : elem.value;
                        }
                        
                        document.getElementById("newContact").value = result + 1;
                    }
                    
                </script>
                
    </body>
</html>
