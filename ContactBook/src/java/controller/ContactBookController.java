/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.ContactBookDAO;
import model.pojo.ContactBook;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ContactBookController implements Controller{

    private static Logger logger = Logger.getGlobal();
    
    @Override
    public ModelAndView handleRequest(HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        ModelAndView mv = new ModelAndView("contacts");
        
        if (hsr.getMethod().equalsIgnoreCase("POST")){
            if (hsr.getParameter("isDeleted") != null) { 
                ContactBookDAO.Delete(
                    Integer.valueOf(hsr.getParameter("id"))
                ); 
            } else {
                ContactBook newContact = new ContactBook(
                    Integer.valueOf(hsr.getParameter("id")),
                    hsr.getParameter("firstName"),
                    hsr.getParameter("secondName"),
                    hsr.getParameter("lastName"),
                    hsr.getParameter("phone"),
                    hsr.getParameter("address"),
                    hsr.getParameter("workPhone"),
                    hsr.getParameter("workPlace")
                );
            
                ContactBookDAO.Write(newContact);
            }
        }
        
        try {
            List<ContactBook> lst = ContactBookDAO.Search(hsr.getParameter("search"));
            mv.addObject("contacts", lst);            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return mv;
    }
    
}
