/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.pojo.ContactBook;
import model.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Admin
 */
public class ContactBookDAO {
    public static List<ContactBook> ReadContactBook(){
        List<ContactBook> lst = null;
        
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            String hql = "from ContactBook";
            Query query = session.createQuery(hql);
            lst = query.list();
            
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lst;
    }
    
    public static void Write(ContactBook newContact){
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            session.beginTransaction();
            session.saveOrUpdate(newContact);
            session.getTransaction().commit();
            
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void Delete(int id){
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            
            String hql = "delete from ContactBook CB where CB.id = ".concat(String.valueOf(id));
            Query query = session.createQuery(hql);
            query.executeUpdate();
            
            session.beginTransaction();
            session.getTransaction().commit();
            
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static List<ContactBook> Search(String searchString){
        List<ContactBook> lst = new ArrayList<>();
        
        if(searchString == null) return ReadContactBook();
        
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            List<ContactBook> lstBeforSearch = null;
            
            String hql = "from ContactBook";
            Query query = session.createQuery(hql);
            lstBeforSearch = query.list();
            
            ArrayList<String> arForSearch = new ArrayList<>();
            while (searchString.length() > 0){
                int n = 0;
                while (searchString.length() > n && searchString.charAt(n) != ' ' ){
                    n++;
                }
                arForSearch.add(searchString.substring(0, n).trim());
                searchString = searchString.substring(n).trim();
            }
            
            for (ContactBook contact : lstBeforSearch) {
                String fio =
                        contact.getFirstName() + " " +
                        contact.getSecondName() + " " +
                        contact.getLastName();
                        
                for (String stringForSearch : arForSearch) {
                    if(fio.contains(stringForSearch)){
                        lst.add(contact);
                        break;
                    }
                }
            }
            
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lst;
    } 
    
}
