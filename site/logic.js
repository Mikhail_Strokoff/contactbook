function initSession() {
	
	var tableData = new Array("90", "100", "201");
	createTable(tableData);
	
}

function sendRequest(data_for_request) {
	
	var req = new XMLHttpRequest();
	
	req.open("POST", "127.0.0.1:8080");
	req.onreadystatechange = processReqChange;
    req.send(data_for_request);
	
}

function processReqChange() {
	
	var tableData;
	
	try { // Важно!
	// только при состоянии "complete"
		if (req.readyState == 4) {
			// для статуса "OK"
			if (req.status == 200) {
				tableData = req.responsetext.parse();
			} else {
				alert("Не удалось получить данные:\n" +
					req.statusText);
					return;
			}
		} else {
			return;
		}
	}
	catch( e ) {
		alert(e);
		return;
	}
	
	createTable(tableData);
	
}

function createTable(tableData) {
	
	var table = document.getElementById("bookContentTableId");
	table.innerHTML = "";
	
		if (tableData != null){
			
			for (var i = 0; i < tableData.length; i++){
				var row = "<tr>"
					row += "<td><input /></td>"
					row += "<td><input /></td>"
					row += "<td><input /></td>"
					row += "<td>" + tableData[i] + "</td>"
				row += "</tr>";
				table.innerHTML += row;
			}
			
		}
			
}